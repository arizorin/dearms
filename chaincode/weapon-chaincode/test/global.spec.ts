import { ChaincodeMockStub, Transform} from '@theledger/fabric-mock-stub';
import { expect } from 'chai';
import {ChaincodeStub, Shim} from 'fabric-shim';
import {WeaponContract} from '../src';

export class Chaincode extends WeaponContract {
        public async Init(stub: ChaincodeStub) {
            console.info('INIT');
            const args = stub.getArgs();

            if (args[0] === 'init') {
                // await this.initLedger(stub, args);
            }

            return Shim.success(Buffer.from(JSON.stringify({
                args,
            })));
        }
        public async Invoke(stub: ChaincodeStub) {
            const request = stub.getFunctionAndParameters();
            const method = this[request.fcn];
            const ctx = {
                stub,
                clientIdentity: {
                    getID: () => {
                        return stub.getCreator();
                    },
                },
            };
            if (!method) {
                console.log('Method ' + request.fcn + ' not found');
                throw new Error('Received unknown function ' + request.fcn + ' invocation');
            }
            try {
                const payload = await method(ctx, ...request.params);
                return Shim.success(payload);
            } catch (err) {
                return Shim.error(err);
            }
        }
}

const chaincode = new Chaincode();
describe('Тесты основного контракта', () => {
    it('Оружие добавлено в блокчейн', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const weapon = {
            name: 'TG3 исп. 01',
              description:
                'Самозарядный гладкоствольный карабин TG3 под патрон 9,6/53 Lancaster',
              serialNumber: 'str12345',
              ownerPassport: '1234666999',
              createdAt: new Date().toString(),
        };
        const response = await mockStub.mockInvoke('txID1', ['AddWeapon', JSON.stringify(weapon)]);
        expect(response.status).to.eql(200);
    });
    it('Оружие получено из блокчейна', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const weapon = {
            name: 'TG3 исп. 01',
            description:
                'Самозарядный гладкоствольный карабин TG3 под патрон 9,6/53 Lancaster',
            serialNumber: 'str12345',
            ownerPassport: '1234666999',
            createdAt: new Date().toString(),
        };
        const response = await mockStub.mockInvoke('txID1', ['AddWeapon', JSON.stringify(weapon)]);
        const response1 = await mockStub.mockInvoke('txID2', ['GetWeapon', weapon.serialNumber]);
        expect(response1.status).to.eql(200);
    });
    it('Оружие не обновлено в блокчейне, 500', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const weapon = {
            name: 'TG3 исп. 01',
            description:
                'Самозарядный гладкоствольный карабин TG3 под патрон 9,6/53 Lancaster',
            serialNumber: 'str12345',
            ownerPassport: '1234666999',
            createdAt: new Date().toString(),
        };
        const response1 = await mockStub.mockInvoke('txID2', ['UpdateWeapon', JSON.stringify(weapon)]);
        expect(response1.status).to.eql(500);
    });
    it('Оружие не передано другому владельцу, 500', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const weapon = {
            name: 'TG3 исп. 01',
            description:
                'Самозарядный гладкоствольный карабин TG3 под патрон 9,6/53 Lancaster',
            serialNumber: 'str12345',
            ownerPassport: '1234666999',
            createdAt: new Date().toString(),
        };
        const response = await mockStub.mockInvoke('txID1', ['AddWeapon', JSON.stringify(weapon)]);
        const response1 = await mockStub.mockInvoke('txID1', ['TransferWeapon', 'str12345', '1234666000']);
        expect(response1.status).to.eql(500);
    });
    it('Выполнен поиск по блокчейну', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const query = {
            selector: {},
        };
        const response1 = await mockStub.mockInvoke('txID1', ['QueryWeapons', JSON.stringify(query)]);
        expect(response1.status).to.eql(200);
    });
    it('Получения полная история транзакций по ключу, 500', async () => {
        const mockStub = new ChaincodeMockStub('Weapon', chaincode);
        const response1 = await mockStub.mockInvoke('txID1', ['GetHistoryForKey', 'str12345']);
        expect(response1.status).to.eql(200);
    });
});
