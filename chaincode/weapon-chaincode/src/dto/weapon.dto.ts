
export interface AddWeaponDto {
    serialNumber: string; // Серийный номер
    ownerPassport: string; // Владелец (паспорт)
    name: string; // Наименование
    description?: string; // Описание
    meta?: string; //мета теги
    createdAt: string;
}


export interface UpdateWeaponDto {
    serialNumber: string; // Серийный номер
    ownerPassport: string; // Владелец (паспорт)
    name: string; // Наименование
    description?: string; // Описание
    meta?: string; //мета теги
    updatedAt: string;
}