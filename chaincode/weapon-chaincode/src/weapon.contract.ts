import {Context, Contract, Info, Returns, Transaction} from 'fabric-contract-api';
import { AddWeaponDto } from './dto/weapon.dto';
import {Weapon} from './model/weapon.model';

@Info({title: 'Weapon', description: 'Smart contract for weapon registry'})
export class WeaponContract extends Contract {

    // AddWeapon - постановка оружия на учет
    @Transaction()
    public async AddWeapon(ctx: Context, data: string): Promise<void> {
        const newWeaponJson: AddWeaponDto = JSON.parse(data);
        const currentWeapon = String(await ctx.stub.getState(newWeaponJson.serialNumber));
        let weapon: Weapon;
        if (currentWeapon) {
            weapon = JSON.parse(currentWeapon);
            const isFlowValid = JSON.parse((await ctx.stub.invokeChaincode('status', ['isTransitionValid', weapon.status, 'НА УЧЕТЕ'], 'mychannel')).payload.toString());
            console.log('[FLOW-VALIDATION]', isFlowValid);
            if (!isFlowValid) {
                throw new Error(`Оружие с серийным номером - ${weapon.serialNumber} не может быть поставлено на учет,
                 так как его текущий статус ${weapon.status}`);
            }
            weapon.ownerPassport = newWeaponJson.ownerPassport;
            weapon.editor = ctx.clientIdentity.getID();
            weapon.updatedAt = newWeaponJson.createdAt;
            weapon.status = 'НА УЧЕТЕ';
        } else {
            weapon = {
                ...newWeaponJson,
                creator: ctx.clientIdentity.getID(),
                status: 'НА УЧЕТЕ',
            };
        }
        await ctx.stub.putState(newWeaponJson.serialNumber, Buffer.from(JSON.stringify(weapon)));
    }

    // GetWeapon - получить оружие по серийному номеру
    @Transaction(false)
    public async GetWeapon(ctx: Context, serialNumber: string): Promise<string> {
        const assetJSON = await ctx.stub.getState(serialNumber);
        if (!assetJSON || assetJSON.length === 0) {
            throw new Error(`Оружие с серийным номером ${serialNumber} в системе не найдено`);
        }
        return assetJSON.toString();
    }

    // UpdateWeapon - обновить информациб об ед. оружия
    @Transaction()
    public async UpdateWeapon(ctx: Context, data: string): Promise<void> {
        const {serialNumber, creator, status, createdAt, ...weaponJson} = JSON.parse(data);

        const currentWeapon = JSON.parse(await this.GetWeapon(ctx, serialNumber));
        if (!currentWeapon) {
            throw new Error(`Оружие с серийным номером ${serialNumber} в системе не найдено`);
        }

        const updatedWeapon: Weapon = {
           ...currentWeapon,
           ...weaponJson,
           editor: ctx.clientIdentity.getID(),
        };

        return ctx.stub.putState(serialNumber, Buffer.from(JSON.stringify(updatedWeapon)));
    }

    // ChangeWeaponStatus - смена статуса у ед. оружия
    @Transaction()
    public async ChangeWeaponStatus(ctx: Context, serialNumber: string, status: string, updatedAt: string): Promise<void> {
        const currentWeapon = JSON.parse(await this.GetWeapon(ctx, serialNumber));
        if (!currentWeapon) {
            throw new Error(`Оружие с серийным номером ${serialNumber} в системе не найдено`);
        }
        const isFlowValid = JSON.parse((await ctx.stub.invokeChaincode('status', ['isTransitionValid', currentWeapon.status, status], 'mychannel')).payload.toString());
        console.log('[FLOW-VALIDATION]', isFlowValid);
        if (!isFlowValid) {
            throw new Error(`Данный статус не может быть установлен`);
        }

        const updatedWeapon: Weapon = {
            ...currentWeapon,
            updatedAt,
            editor: ctx.clientIdentity.getID(),
            status,
         };

        return ctx.stub.putState(serialNumber, Buffer.from(JSON.stringify(updatedWeapon)));
    }

    // WeaponExists - проверка наличия оружия в бч
    @Transaction(false)
    @Returns('boolean')
    public async WeaponExists(ctx: Context, serialNumber: string): Promise<boolean> {
        const assetJSON = await ctx.stub.getState(serialNumber);
        return assetJSON && assetJSON.length > 0;
    }

    // TransferWeapon - передать оружия от одного владельца к другому
    @Transaction()
    public async TransferWeapon(ctx: Context, serialNumber: string, newOwner: string, updatedAt: string): Promise<void> {
        const weaponString = await this.GetWeapon(ctx, serialNumber);
        const weapon: Weapon = JSON.parse(weaponString);
        if (['УНИЧТОЖЕНО', 'ВЫВЕЗЕНО', 'В РОЗЫСКЕ'].includes(weapon.status)) {
            throw new Error(`Оружие с серийным номером ${serialNumber} не может быть передано в связи с его текущим статусом`);
        }

        weapon.ownerPassport = newOwner;
        weapon.status = 'НА УЧЕТЕ';
        weapon.updatedAt = updatedAt;
        weapon.editor = ctx.clientIdentity.getID();

        await ctx.stub.putState(serialNumber, Buffer.from(JSON.stringify(weapon)));
    }

    // QueryWeapons - получить данные
    @Transaction(false)
    @Returns('string')
    public async QueryWeapons(ctx: Context, query: string): Promise<string> {
        const allResults = [];
        const iterator = await ctx.stub.getQueryResult(query);
        let result = await iterator.next();
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            allResults.push(record);
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    // GetHistoryForKey - история операций
    @Transaction(false)
    @Returns('string')
    public async GetHistoryForKey(ctx: Context, serialNumber: string): Promise<string> {
        const promiseOfIterator = ctx.stub.getHistoryForKey(serialNumber);
        const results = [];
        for await (const keyMod of promiseOfIterator) {
            const resp = {
                timestamp: keyMod.timestamp,
                txid: keyMod.txId,
                data: '',
            };

            if (keyMod.isDelete) {
                resp.data = 'УДАЛЕНО';
            } else {
                resp.data = keyMod.value.toString();
            }
            results.push(resp);
        }
        return JSON.stringify(results);
    }
}
