export enum Statuses {
    INITED,
    REGISTERED,
    DEREGISTERED,
    INSEARCH,
    DESTROYED
  }