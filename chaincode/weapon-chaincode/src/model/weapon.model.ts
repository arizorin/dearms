import {Object, Property} from 'fabric-contract-api';
import { Statuses } from './status.model';


@Object()
export class Weapon {
    @Property()
    public serialNumber: string; // Серийный номер

    @Property()
    public ownerPassport: string; // Владелец (паспорт)

    @Property()
    public name: string; // Наименование

    @Property()
    public description?: string; // Описание

    @Property()
    public meta?: string; //мета теги

    @Property()
    public status: string; // статусы

    @Property()
    public createdAt: string; // время создания

    @Property()
    public creator: string; // создатель

    @Property()
    public updatedAt?: string; // время обновления

    @Property()
    public editor?: string; // редактор
}
