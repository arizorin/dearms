import {WeaponContract} from './weapon.contract';

export {WeaponContract} from './weapon.contract';

export const contracts: any[] = [WeaponContract];
