import {Context, Contract, Info, Transaction} from 'fabric-contract-api';
import {name as KEY} from '../package.json';
import {IStatus} from './model/status.model';

@Info({title: 'Status', description: 'Smart contract for status flow'})
export class StatusContract extends Contract {

    @Transaction()
    public async InitLedger(ctx: Context): Promise<void> {
        const initialStatusMap: IStatus[] = [
            {
                name: 'В СИСТЕМЕ',
                next: ['НА УЧЕТЕ', 'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
            },
            {
                name: 'НА УЧЕТЕ',
                next: ['СНЯТО С УЧЕТА'],
            },
            {
                name: 'СНЯТО С УЧЕТА',
                next: ['НА УЧЕТЕ',  'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
            },
            {
                name: 'УНИЧТОЖЕНО',
                next: [],
            },
            {
                name: 'ВЫВЕЗЕНО',
                next: ['ВВЕЗЕНО'],
            },
        ];
        await ctx.stub.putState(KEY, Buffer.from(JSON.stringify(initialStatusMap)));
    }

    @Transaction()
    public async SetStatusFlow(ctx: Context, statusMap: string): Promise<void> {
        const statuses = JSON.parse(statusMap);
        if (!statuses) {
            throw new Error(`Некорректная карта переходов`);
        }
        await ctx.stub.putState(KEY, Buffer.from(JSON.stringify(statuses)));
    }

    @Transaction(false)
    public async GetStatusFlow(ctx: Context): Promise<string> {
        const statusMap = await ctx.stub.getState(KEY);
        if (!statusMap || statusMap.length === 0) {
            throw new Error(`Карта переходов в системе не найдена`);
        }
        return statusMap.toString();
    }

    @Transaction(false)
    public async GetNextStatuses(ctx: Context, currentStatus: string): Promise<string> {
        const statusMap = JSON.parse(await this.GetStatusFlow(ctx));
        const nextStatuses = statusMap?.find((statusObject: IStatus) => {
            return statusObject.name === currentStatus;
        });
        return JSON.stringify(nextStatuses);
    }

    @Transaction(false)
    public async isTransitionValid(ctx: Context, currentStatus: string, nextStatus: string): Promise<string> {
        if (!currentStatus || !nextStatus) {
            throw new Error(`Входные данные некорректны`);
        }
        const statusMap = JSON.parse(await this.GetStatusFlow(ctx));
        const currentStatusObject = statusMap.find((statusObject: IStatus) => statusObject.name === currentStatus);
        if (!currentStatusObject) {
            throw new Error(`Текущий статус не найден`);
        }
        return currentStatusObject.next.includes(nextStatus).toString();
    }
}
