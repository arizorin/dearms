export interface IStatus {
    name: string;
    next: string[];
}
