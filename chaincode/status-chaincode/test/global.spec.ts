import { ChaincodeMockStub, Transform} from '@theledger/fabric-mock-stub';
import { expect } from 'chai';
import {ChaincodeStub, Shim} from 'fabric-shim';
import {StatusContract} from '../src';
import {IStatus} from '../src/model/status.model';

export class Chaincode extends StatusContract {
        public async Init(stub: ChaincodeStub) {
            console.info('INIT');
            const args = stub.getArgs();

            if (args[0] === 'init') {
                // await this.initLedger(stub, args);
            }

            return Shim.success(Buffer.from(JSON.stringify({
                args,
            })));
        }

        public async Invoke(stub: ChaincodeStub) {
            const request = stub.getFunctionAndParameters();
            const method = this[request.fcn];
            const ctx = {
                stub,
            };

            if (!method) {
                console.log('Method ' + request.fcn + ' not found');
                throw new Error('Received unknown function ' + request.fcn + ' invocation');
            }
            try {
                const payload = await method(ctx, ...request.params);
                return Shim.success(payload);
            } catch (err) {
                return Shim.error(err);
            }
        }
}

const chaincode = new Chaincode();
describe('Тесты статусной модели', () => {
    it('Чейнкод должен проиницилизироваться', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const response = await mockStub.mockInvoke('txID1', ['InitLedger']);
        expect(response.status).to.eql(200);
    });
    it('Должна вернуться статусная модель', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const response1 = await mockStub.mockInvoke('txID1', ['InitLedger']);
        const response = await mockStub.mockInvoke('txID2', ['GetStatusFlow']);
        expect(response.status).to.eql(200);
    });
    it('Должна установиться новая статусная модель', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const statusMap: IStatus[] = [
            {
                name: 'В СИСТЕМЕ',
                next: ['НА УЧЕТЕ',  'В РОЗЫСКЕ', 'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
            },
            {
                name: 'ВЫВЕЗЕНО',
                next: ['ВВЕЗЕНО'],
            },
        ];
        const response = await mockStub.mockInvoke('txID1', ['SetStatusFlow', JSON.stringify(statusMap)]);
        expect(response.status).to.eql(200);
    });
    it('Должны вернуться следующие статусы по статусной модели', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const statusMap: IStatus[] = [
            {
                name: 'В СИСТЕМЕ',
                next: ['НА УЧЕТЕ',  'В РОЗЫСКЕ', 'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
            },
            {
                name: 'ВЫВЕЗЕНО',
                next: ['ВВЕЗЕНО'],
            },
        ];
        const response1 = await mockStub.mockInvoke('txID1', ['SetStatusFlow', JSON.stringify(statusMap)]);
        const response2 = await mockStub.mockInvoke('txID2', ['GetNextStatuses', 'В СИСТЕМЕ']);
        const nextStatuses = JSON.parse(response2.payload.toString()).next;
        expect(nextStatuses).to.eql(statusMap[0].next);
        expect(response2.status).to.eql(200);
    });
    it('Должно вернуться 200, true', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const response1 = await mockStub.mockInvoke('txID1', ['InitLedger']);
        const response2 = await mockStub.mockInvoke('txID2', ['isTransitionValid', 'В СИСТЕМЕ', 'НА УЧЕТЕ']);
        expect(Boolean(response2.payload.toString())).to.eql(true);
        expect(response2.status).to.eql(200);
    });
    it('Должно вернуться 200, false', async () => {
        const mockStub = new ChaincodeMockStub('Status', chaincode);
        const response1 = await mockStub.mockInvoke('txID1', ['InitLedger']);
        const response2 = await mockStub.mockInvoke('txID2', ['isTransitionValid', 'УНИЧТОЖЕНО', 'НА УЧЕТЕ']);
        expect(Boolean(response2.payload.toString())).to.eql(false);
        expect(response2.status).to.eql(200);
    });
});
