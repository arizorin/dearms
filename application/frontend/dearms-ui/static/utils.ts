interface INavigationLink {
  name: string
  link: string
  icon?: string
  roles: number[]
}

export const navigationLinks: INavigationLink[] = [
  {
    name: 'Профиль',
    link: '/profile',
    icon: 'mdi-account-outline',
    roles: [],
  },
  {
    name: 'Дашборд',
    link: '/dashboard',
    icon: 'mdi-view-dashboard',
    roles: [2, 3],
  },
  {
    name: 'Заявления',
    link: '/manage',
    icon: 'mdi-clipboard-edit-outline',
    roles: [2, 3],
  },
  {
    name: 'Оружие на учете',
    link: '/search',
    icon: 'mdi-eye-outline',
    roles: [3],
  },
  {
    name: 'Услуги',
    link: '/service',
    icon: 'mdi-card-account-details-outline',
    roles: [1],
  },
]

export const getStatusToHuman = (status: string): string => {
  return status
}

export const getUrlParamsFromObject = (obj: Object) =>
  Object.entries(obj)
    .map(([key, val]) => `${key}=${val}`)
    .join('&')
