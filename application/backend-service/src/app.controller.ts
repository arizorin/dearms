import { Controller, Get } from '@nestjs/common';
import { version } from '../package.json';

@Controller()
export class AppController {
  constructor() {}

  @Get()
  getHello(): string {
    return `<h1>DE ARMS API ${version}</h1>
            <h2>${new Date()}</h2>`;
  }
}
