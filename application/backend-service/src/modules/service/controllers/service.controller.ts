import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ServiceService } from '../services/service.service';
import { RegisterWeaponServiceDto } from '../dtos/register-weapon-service.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DeRegisterWeaponServiceDto } from '../dtos/deregister-weapon-service.dto';
import { User } from '../../user/decorators/user.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../user/guards/roles.guard';
import { Roles } from '../../user/decorators/role.decorator';
import { UserRole } from '../../user/model/user.model';
import { QueryServiceDto } from '../dtos/query-service.dto';

@Controller('service')
@ApiTags('Service')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
export class ServiceController {
  constructor(private service: ServiceService) {}

  @Get('list')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getAllServices(@Query() queryServiceDto: QueryServiceDto) {
    return this.service.getAllServices(queryServiceDto);
  }

  @Get('types')
  async getAllServiceTypes() {
    return this.service.getAllServiceTypes();
  }

  @Get('statuses')
  async getAllServiceStatuses() {
    return this.service.getAllServiceStatuses();
  }

  @Get('latest')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  getLatestService() {
    return this.service.getLatestService();
  }

  @Get(':id')
  async getServiceById(@Param('id') id: number) {
    return this.service.getServiceById(id);
  }

  @Put(':id')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async changeServiceStatusById(
    @Param('id') id: number,
    @Body('statusId') statusId: number,
  ) {
    return this.service.changeServiceStatus(id, statusId);
  }

  @Post('apply-register-weapon')
  @Roles(UserRole.USER)
  async applyRegisterWeapon(
    @Body() registerWeaponServiceDto: RegisterWeaponServiceDto,
    @User() user,
  ) {
    return this.service.registerWeaponService(
      registerWeaponServiceDto,
      user.id,
    );
  }

  @Post('apply-deregister-weapon')
  @Roles(UserRole.USER)
  async applyDeRegisterWeapon(
    @Body() deregisterWeaponServiceDto: DeRegisterWeaponServiceDto,
    @User() user,
  ) {
    return this.service.deRegisterWeaponService(
      deregisterWeaponServiceDto,
      user.id,
    );
  }
  @Get('')
  @Roles(UserRole.USER)
  async getUserServices(@User() user) {
    return this.service.getServicesByUserId(user.id);
  }

  @Get('/user/:id')
  @Roles(UserRole.MODERATOR, UserRole.ADMINISTRATOR)
  async getUserServicesByUserId(@Param('id') id: number) {
    return this.service.getServicesByUserId(id);
  }
}
