import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Connection, getConnection, Repository } from 'typeorm';
import { ServiceEntity } from '../entities/service.entity';
import { ServiceTypeEntity } from '../entities/service-type.entity';
import { RegisterWeaponServiceDto } from '../dtos/register-weapon-service.dto';
import { WeaponEntity } from '../../weapon/weapon.entity';
import { ServiceTypes } from '../models/service.model';
import { DeRegisterWeaponServiceDto } from '../dtos/deregister-weapon-service.dto';
import { ServiceStatusEntity } from '../entities/service-status.entity';
import { QueryServiceDto } from '../dtos/query-service.dto';
import { BlockchainService } from '../../blockchain/blockchain.service';

@Injectable()
export class ServiceService {
  constructor(
    @InjectRepository(ServiceEntity)
    private readonly serviceRepository: Repository<ServiceEntity>,
    @InjectRepository(ServiceTypeEntity)
    private readonly serviceTypeRepository: Repository<ServiceTypeEntity>,
    @InjectRepository(ServiceStatusEntity)
    private readonly serviceStatusRepository: Repository<ServiceStatusEntity>,
    @InjectRepository(WeaponEntity)
    private readonly weaponEntityRepository: Repository<WeaponEntity>,
    private readonly connection: Connection,
    private blockchainService: BlockchainService,
  ) {}

  async getAllServices(queryServiceDto: QueryServiceDto) {
    const relationsMapping = {
      name: 'user',
      last_name: 'user',
      middle_name: 'user',
      service_type_id: 'service',
      status_id: 'service',
      serial: 'passport',
      number: 'passport',
      created_at: 'service',
      updated_at: 'service',
    };

    const result = await this.serviceRepository
      .createQueryBuilder('service')
      .leftJoinAndSelect('service.service_type', 'type')
      .leftJoinAndSelect('service.user', 'user')
      .leftJoinAndSelect('user.passport', 'passport')
      .leftJoinAndSelect('service.weapon', 'weapon')
      .leftJoinAndSelect('service.status', 'status')
      .where(
        new Brackets((qb) => {
          Object.keys(queryServiceDto).map((key) => {
            return qb.where(
              `${
                relationsMapping[key] ? `${relationsMapping[key]}.` : ''
              }${key} = :${key}`,
              {
                [key]: queryServiceDto[key],
              },
            );
          });
        }),
      )
      .getMany();
    return result;
  }

  getServiceById(id: number): Promise<ServiceEntity> {
    return this.serviceRepository.findOne(
      { id: id },
      {
        relations: [
          'service_type',
          'status',
          'weapon',
          'user',
          'user.passport',
          'user.role',
          'user.status',
        ],
      },
    );
  }

  async changeServiceStatus(id: number, statusId: number) {
    const service = await this.serviceRepository.findOne(
      {
        id: id,
        status_id: 1,
      },
      { relations: ['weapon', 'user', 'user.passport'] },
    );
    await this.serviceRepository.save({
      ...service,
      status_id: statusId,
    });

    // Если заявление удв
    if (statusId === 2) {
      // Постановка на учет
      if (service.service_type_id === 1) {
        await this.blockchainService.addWeapon({
          serialNumber: service.weapon.serial_number,
          createdAt: new Date().toString(),
          name: service.weapon.name,
          ownerPassport: `${service.user.passport.serial}${service.user.passport.number}`,
        });
      } else {
        await this.blockchainService.changeWeaponStatus(
          service.weapon.serial_number,
          'СНЯТО С УЧЕТА',
        );
      }
    }
    return service;
  }

  getAllServiceTypes(): Promise<ServiceTypeEntity[]> {
    return this.serviceTypeRepository.find();
  }

  getAllServiceStatuses(): Promise<ServiceStatusEntity[]> {
    return this.serviceStatusRepository.find();
  }

  // Постановка на учет
  async registerWeaponService(
    { serialNumber, name, description }: RegisterWeaponServiceDto,
    userId: number,
  ) {
    return await this.createServiceApplication({
      serialNumber,
      name,
      description,
      userId,
      serviceTypeId: ServiceTypes.WEAPON_REGISTRATION,
    });
  }

  // Снятие с учета
  async deRegisterWeaponService(
    { serialNumber, name, description }: DeRegisterWeaponServiceDto,
    userId: number,
  ) {
    return await this.createServiceApplication({
      serialNumber,
      name,
      description,
      userId,
      serviceTypeId: ServiceTypes.WEAPON_DEREGISTER,
    });
  }

  // Создание заявки
  private async createServiceApplication({
    serialNumber,
    name,
    description,
    userId,
    serviceTypeId,
  }) {
    return await getConnection().transaction(
      async (transactionalEntityManager) => {
        let currentWeapon = await this.weaponEntityRepository.findOne({
          serial_number: serialNumber,
        });
        console.log(currentWeapon);
        if (!currentWeapon) {
          currentWeapon = new WeaponEntity({
            name,
            serialNumber,
            description,
          });
          await transactionalEntityManager.save(currentWeapon);
        }
        return await transactionalEntityManager.save(
          new ServiceEntity({
            weapon: currentWeapon,
            user_id: userId,
            service_type_id: serviceTypeId,
          }),
        );
      },
    );
  }

  // Заявки для юзера
  async getServicesByUserId(userId: number) {
    return await this.serviceRepository.find({
      where: {
        user_id: userId,
      },
      relations: ['status', 'weapon', 'service_type'],
    });
  }

  // async checkWeapon(serial_number: string) {
  //   const result = await this.weaponEntityRepository.find({ serial_number });
  //   if (result) {
  //     throw new HttpException('Weapon already exists', HttpStatus.CONFLICT);
  //   }
  // }

  async getLatestService() {
    return await this.serviceRepository.find({
      take: 10,
      order: {
        created_at: 'DESC',
        id: 'DESC',
      },
      relations: ['status', 'weapon', 'service_type', 'user'],
    });
  }
}
