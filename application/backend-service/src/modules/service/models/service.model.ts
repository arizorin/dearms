// Возможные виды заявок
export enum ServiceTypes {
  WEAPON_REGISTRATION = 1,
  WEAPON_DEREGISTER,
  WEAPON_LOST,
  WEAPON_CHANGE_OWNER,
}

// Возможные статусы заявки
export enum ServiceStatus {
  CREATED = 1,
  REJECTED,
  CONFIRMED,
  DELETED,
}
