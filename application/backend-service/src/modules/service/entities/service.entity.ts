import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from '../../user/entities/user.entity';
import { WeaponEntity } from '../../weapon/weapon.entity';
import { ServiceTypeEntity } from './service-type.entity';
import { ServiceStatusEntity } from './service-status.entity';
import { ServiceStatus } from '../models/service.model';

@Entity('Service')
export class ServiceEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer', nullable: true })
  service_type_id: number;

  @ManyToOne(() => ServiceTypeEntity, (service_type) => service_type.id)
  @JoinColumn({ name: 'service_type_id' })
  service_type: ServiceTypeEntity;

  @Column({ type: 'integer', nullable: true })
  user_id: number;

  @ManyToOne(() => UserEntity, (user) => user.id)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({ type: 'integer', nullable: true })
  weapon_id: number;

  @ManyToOne(() => WeaponEntity, (weapon) => weapon.id)
  @JoinColumn({ name: 'weapon_id' })
  weapon: WeaponEntity;

  @Column({ type: 'integer', nullable: true, default: ServiceStatus.CREATED })
  status_id: number;

  @ManyToOne(() => ServiceStatusEntity, (status) => status.id)
  @JoinColumn({ name: 'status_id' })
  status: ServiceStatusEntity;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  constructor(data: {
    user_id: number;
    weapon: WeaponEntity;
    service_type_id: number;
  }) {
    this.user_id = data?.user_id;
    this.weapon = data?.weapon;
    this.service_type_id = data?.service_type_id;
  }
}
