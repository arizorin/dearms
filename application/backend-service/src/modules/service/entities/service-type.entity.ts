import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('ServiceType')
export class ServiceTypeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description: string;
}
