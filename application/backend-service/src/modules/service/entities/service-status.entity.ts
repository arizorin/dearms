import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('ServiceStatus')
export class ServiceStatusEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description: string;
}
