import {IsNumber, IsNumberString, IsOptional, IsString} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QueryServiceDto {
  @IsOptional()
  @IsString()
  @ApiProperty()
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  last_name: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  middle_name: string;

  @IsOptional()
  @IsNumberString()
  @ApiProperty()
  service_type_id: number;

  @IsOptional()
  @IsNumberString()
  @ApiProperty()
  status_id: number;

  @IsOptional()
  @IsString()
  @ApiProperty()
  created_at: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  updated_at: string;
}
