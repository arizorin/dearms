import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WeaponEntity } from '../weapon/weapon.entity';
import { ServiceEntity } from './entities/service.entity';
import { ServiceController } from './controllers/service.controller';
import { ServiceService } from './services/service.service';
import { ServiceTypeEntity } from './entities/service-type.entity';
import { ServiceStatusEntity } from './entities/service-status.entity';
import { BlockchainModule } from '../blockchain/blockchain.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      WeaponEntity,
      ServiceEntity,
      ServiceTypeEntity,
      ServiceStatusEntity,
    ]),
    BlockchainModule,
  ],
  controllers: [ServiceController],
  providers: [ServiceService],
})
export class ServiceModule {}
