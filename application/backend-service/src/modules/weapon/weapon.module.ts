import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WeaponEntity } from './weapon.entity';
import { WeaponController } from './weapon.controller';
import { WeaponService } from './weapon.service';
import { UserEntity } from '../user/entities/user.entity';
import { BlockchainModule } from '../blockchain/blockchain.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([WeaponEntity, UserEntity]),
    BlockchainModule,
  ],
  controllers: [WeaponController],
  providers: [WeaponService],
  exports: [WeaponService],
})
export class WeaponModule {}
