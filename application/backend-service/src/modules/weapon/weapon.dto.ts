import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional, IsString } from 'class-validator';

export class QueryWeaponDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
  serialNumber: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  ownerPassport: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  status: string;

  @ApiProperty()
  @IsOptional()
  @IsDate()
  createdAt: string;

  @ApiProperty()
  @IsOptional()
  @IsDate()
  updatedAt: string;
}
