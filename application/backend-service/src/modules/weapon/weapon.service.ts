import { Injectable } from '@nestjs/common';
import { BlockchainService } from '../blockchain/blockchain.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../user/entities/user.entity';
import { Repository } from 'typeorm';
import { QueryWeaponDto } from './weapon.dto';

@Injectable({})
export class WeaponService {
  constructor(
    private blockchainService: BlockchainService,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async getUserWeaponsById(id: number) {
    const currentUser = await this.userRepository.findOne(
      { id },
      { relations: ['passport'] },
    );
    const weapons = await this.blockchainService.queryWeapons({
      selector: {
        ownerPassport: `${currentUser.passport.serial}${currentUser.passport.number}`,
      },
    });
    return weapons;
  }

  async getWeaponBySerialNumber(serialNumber: string) {
    return await this.blockchainService.queryWeapons({
      selector: {
        serialNumber,
      },
    });
  }

  async getWeaponHistoryBySerialNumber(serialNumber: string) {
    const result = await this.blockchainService.getWeaponHistory(serialNumber);
    if (!result) {
      return [];
    }
    return result.map((weapon) => {
      return {
        timestamp: new Date(Number(weapon.timestamp.seconds + '000')),
        tx: weapon.txid,
        weapon: JSON.parse(weapon.data),
      };
    });
  }

  async transferWeapon(serialNumber: string, newOwnerPassport: string) {
    return await this.blockchainService.changeWeaponOwner(
      serialNumber,
      newOwnerPassport,
    );
  }

  async changeWeaponStatus(serialNumber: string, newStatus: string) {
    return await this.blockchainService.changeWeaponStatus(
      serialNumber,
      newStatus,
    );
  }

  async searchWeapon(query: QueryWeaponDto) {
    return await this.blockchainService.queryWeapons({
      selector: {
        ...query,
      },
    });
  }

  async getWeaponNextStatuses(currentStatus: string): Promise<string[]> {
    return await this.blockchainService.getNextStatuses(currentStatus);
  }

  async getWeaponAllStatuses(): Promise<string[]> {
    const statusMap = await this.blockchainService.getStatusMap();
    return statusMap.map((status) => status.name);
  }
}
