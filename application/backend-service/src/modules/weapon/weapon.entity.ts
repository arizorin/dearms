import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Weapon')
export class WeaponEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, unique: true })
  serial_number: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description: string;

  constructor(data: {
    name: string;
    serialNumber: string;
    description: string;
  }) {
    this.name = data?.name;
    this.serial_number = data?.serialNumber;
    this.description = data?.description;
  }
}
