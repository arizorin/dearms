import { Controller, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../user/guards/roles.guard';
import { WeaponService } from './weapon.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { User } from '../user/decorators/user.decorator';
import { Roles } from '../user/decorators/role.decorator';
import { UserRole } from '../user/model/user.model';
import { QueryWeaponDto } from './weapon.dto';

@Controller('Weapon')
@ApiTags('Weapon')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
export class WeaponController {
  constructor(private weaponService: WeaponService) {}

  @Get('')
  getUserWeapons(@User() user) {
    return this.weaponService.getUserWeaponsById(user.id);
  }

  @Get('next-status')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  getWeaponNextStatuses(@Query('currentStatus') currentStatus: string) {
    return this.weaponService.getWeaponNextStatuses(currentStatus);
  }

  @Get('all-statuses')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  getWeaponAllStatuses() {
    return this.weaponService.getWeaponAllStatuses();
  }

  @Get('user/:id')
  getUserWeaponsByUserId(@Param('id') id: number) {
    return this.weaponService.getUserWeaponsById(id);
  }

  @Get('search')
  getWeaponsByQuery(@Query() query: QueryWeaponDto) {
    return this.weaponService.searchWeapon(query);
  }

  @Get(':serialNumber')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  getUserWeaponById(@Param('serialNumber') serialNumber: string) {
    return this.weaponService.getWeaponBySerialNumber(serialNumber);
  }

  @Get(':serialNumber/history')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  getWeaponHistoryBySerialNumber(@Param('serialNumber') serialNumber: string) {
    return this.weaponService.getWeaponHistoryBySerialNumber(serialNumber);
  }

  @Post(':serialNumber/transfer')
  @Roles(UserRole.ADMINISTRATOR)
  postTransferWeapon(
    @Param('serialNumber') serialNumber: string,
    @Query('newOwnerPassport') newOwnerPassport: string,
  ) {
    console.log(newOwnerPassport);
    return this.weaponService.transferWeapon(serialNumber, newOwnerPassport);
  }

  @Post(':serialNumber/status')
  @Roles(UserRole.ADMINISTRATOR)
  postChangeWeaponStatus(
    @Param('serialNumber') serialNumber: string,
    @Query('status') status: string,
  ) {
    return this.weaponService.changeWeaponStatus(serialNumber, status);
  }
}
