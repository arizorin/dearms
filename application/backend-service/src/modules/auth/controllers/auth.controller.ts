import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { AuthDto } from '../dtos/auth.dto';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { RolesGuard } from '../../user/guards/roles.guard';
import { UserRole } from '../../user/model/user.model';
import { Roles } from '../../user/decorators/role.decorator';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async authUser(@Body() authDto: AuthDto) {
    return await this.authService.authUser(authDto);
  }

  @Get('latest')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getLatestAuthedUsers() {
    return await this.authService.getLatestAuthedUsers();
  }
}
