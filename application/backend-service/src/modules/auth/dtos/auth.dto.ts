import { IsMobilePhone, IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AuthDto {
  @ApiProperty()
  @IsString()
  @IsMobilePhone('ru-RU')
  phoneNumber: string;

  @ApiProperty()
  @IsString()
  @Length(4, 4)
  smsCode: string;
}
