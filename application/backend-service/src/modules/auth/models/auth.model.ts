import { UserEntity } from '../../user/entities/user.entity';

export interface IAuthLogin {
  accessToken: string;
  user: UserEntity;
}
