import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AuthDto } from '../dtos/auth.dto';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../../user/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccessEntity } from '../entities/access.entity';
import { IAuthLogin } from '../models/auth.model';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(AccessEntity)
    private readonly accessRepository: Repository<AccessEntity>,
    private jwtService: JwtService,
  ) {}

  async authUser(authDto: AuthDto): Promise<string> {
    const currentUser = await this.getUserDetails(authDto.phoneNumber);
    return await this.issueAccessToken(currentUser);
  }

  async issueAccessToken(currentUser: UserEntity): Promise<string> {
    const payload = {
      id: currentUser.id,
      role: currentUser.role_id,
      status: currentUser.status_id,
    };
    const issuedToken = this.jwtService.sign(payload);
    await this.disableAllTokensForUserById(currentUser.id).then(async () => {
      await this.accessRepository.save({
        user_id: currentUser.id,
        token: issuedToken,
      });
    });
    return issuedToken;
  }

  async getUserDetails(phoneNumber: string): Promise<UserEntity> {
    const currentUser = await this.userRepository.findOne({
      phone_number: phoneNumber,
    });
    if (!currentUser) {
      throw new HttpException(
        { message: 'User not found' },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return currentUser;
  }

  async disableAllTokensForUserById(userId: number): Promise<void> {
    await this.accessRepository.update(
      {
        user_id: userId,
      },
      {
        is_valid: false,
      },
    );
  }

  async getLatestAuthedUsers() {
    return await this.accessRepository
      .createQueryBuilder('access')
      .leftJoin('access.user', 'user')
      .leftJoin('user.role', 'role')
      .select(
        'user.id, user.name, user.last_name, user.middle_name, access.issued_at, role.name as role',
      )
      .orderBy('access.issued_at', 'DESC')
      // .limit(10)
      .getRawMany();
  }
}
