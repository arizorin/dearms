import { AddWeaponDto } from '../../../../../chaincode/weapon-chaincode/dist/dto/weapon.dto';

export interface IWeaponBlockchain {
  key: string;
  Record: AddWeaponDto;
}
