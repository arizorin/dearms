import {
  Injectable,
  OnApplicationBootstrap,
  OnApplicationShutdown,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { buildCCPOrg1, buildWallet } from '../../utils/AppUtil';
import {
  buildCAClient,
  enrollAdmin,
  registerAndEnrollUser,
} from '../../utils/CAUtil';
import * as path from 'path';
import {
  Contract,
  Gateway,
  GatewayOptions,
  Network,
  Wallet,
} from 'fabric-network';
import * as FabricCAServices from 'fabric-ca-client';
import {
  AddWeaponDto,
  UpdateWeaponDto,
} from '../../../../../chaincode/weapon-chaincode/dist/dto/weapon.dto';
import { IWeaponBlockchain } from './blockchain.model';

@Injectable({})
export class BlockchainService
  implements OnApplicationBootstrap, OnApplicationShutdown {
  walletPath: string;
  channelName: string;
  weaponChaincodeName: string;
  statusChaincodeName: string;
  mspOrg: string;
  orgUserId: string;

  gateWay: Gateway;
  gatewayOpts: GatewayOptions;
  weaponContract: Contract;
  statusContract: Contract;
  network: Network;
  wallet: Wallet;
  caClient: FabricCAServices;

  constructor(private configService: ConfigService) {
    this.channelName = this.configService.get<string>('CHANNEL_NAME');
    this.weaponChaincodeName = this.configService.get<string>(
      'WEAPON_CHAINCODE',
    );
    this.statusChaincodeName = this.configService.get<string>(
      'STATUS_CHAINCODE',
    );
    this.mspOrg = this.configService.get<string>('MSP_ORG');
    this.orgUserId = this.configService.get<string>('ORG_USER');

    this.walletPath = path.join(__dirname, 'wallet');
  }

  // Подключение к блокчейну
  async connect() {
    console.log('[BLOCKCHAIN] CONNECTION....');
    try {
      const ccp = buildCCPOrg1();
      this.caClient = buildCAClient(ccp, 'ca.org1.example.com');
      this.wallet = await buildWallet(this.walletPath);
      await this.addAdmin();
      await this.addUser();

      this.gateWay = new Gateway();

      this.gatewayOpts = {
        wallet: this.wallet,
        identity: this.orgUserId,
        discovery: { enabled: true, asLocalhost: true },
      };

      await this.gateWay.connect(ccp, this.gatewayOpts);
      this.network = await this.gateWay.getNetwork(this.channelName);
      this.weaponContract = this.network.getContract(this.weaponChaincodeName);
      this.statusContract = this.network.getContract(this.statusChaincodeName);
      console.log('[BLOCKCHAIN] CONNECTED');
      this.test();
    } catch (error) {
      console.error(
        `[BLOCKCHAIN] ******** FAILED to run the application: ${error}`,
      );
      this.disconnect();
    }
  }

  async test() {
    // await this.initStatusMap();
    // const r = await this.isStatusTransitionValid('НА УЧЕТЕ', 'СНЯТО С УЧЕТА');
    // console.log(r);
    // await this.addWeapon({
    //   name: 'TG3 исп. 012',
    //   description:
    //     'Самозарядный гладкоствольный карабин TG3 под патрон 9,6/53 Lancaster',
    //   serialNumber: 'str12346',
    //   ownerPassport: '1234666992',
    //   createdAt: new Date().toString(),
    // });
    // await this.changeWeaponStatus('str12346', 'СНЯТО С УЧЕТА');
    // await this.changeWeaponStatus('str12346', 'НА УЧЕТЕ');
    // const result = await this.getWeaponHistory('str12345');
    await this.changeWeaponOwner('str12346', '8709234349');
    // const result = await this.queryWeapons({
    //   selector: {}
    // });
    // console.log(result);
  }

  onApplicationBootstrap(): any {
    this.connect();
  }

  onApplicationShutdown(signal?: string): any {
    this.disconnect();
  }

  async disconnect() {
    await this.gateWay.disconnect();
    console.log('[BLOCKCHAIN] DISCONNECTED');
  }

  // Добавление пользователя в сеть
  async addUser() {
    await registerAndEnrollUser(
      this.caClient,
      this.wallet,
      this.mspOrg,
      this.orgUserId,
      'org1.department1',
    );
  }

  // Добавление администратора в сеть
  async addAdmin() {
    await enrollAdmin(this.caClient, this.wallet, this.mspOrg);
  }

  // Добавить оружие
  async addWeapon(weapon: AddWeaponDto) {
    await this.weaponContract.submitTransaction(
      'AddWeapon',
      JSON.stringify(weapon),
    );
  }

  // Обновить оружие
  async updateWeapon(weapon: UpdateWeaponDto) {
    await this.weaponContract.submitTransaction(
      'UpdateWeapon',
      JSON.stringify(weapon),
    );
  }

  // Сменить статус у оружия
  async changeWeaponStatus(serialNumber: string, status: string) {
    await this.weaponContract.submitTransaction(
      'ChangeWeaponStatus',
      serialNumber,
      status,
      new Date().toString(),
    );
  }

  // Получить оружие по серийному номеру
  async getWeaponBySerialNumber(
    serialNumber: string,
  ): Promise<IWeaponBlockchain> {
    const weapon = await this.weaponContract.evaluateTransaction(
      'GetWeapon',
      serialNumber,
    );
    return JSON.parse(weapon.toString());
  }

  // Передать оружия от одного владельца к другому
  async changeWeaponOwner(serialNumber: string, newOwnerPassport: string) {
    await this.weaponContract.submitTransaction(
      'TransferWeapon',
      serialNumber,
      newOwnerPassport,
      new Date().toString(),
    );
  }

  // Запрос в блокчейн
  async queryWeapons(
    query: Record<string, unknown>,
  ): Promise<IWeaponBlockchain[]> {
    const result = await this.weaponContract.submitTransaction(
      'QueryWeapons',
      JSON.stringify(query),
    );
    return JSON.parse(result.toString());
  }

  // Получение истории
  async getWeaponHistory(serialNumber: string): Promise<any[]> {
    const result = await this.weaponContract.submitTransaction(
      'GetHistoryForKey',
      serialNumber,
    );
    return JSON.parse(result.toString());
  }

  // Инициализация
  async initStatusMap() {
    await this.statusContract.submitTransaction('InitLedger');
  }

  // Получить статусную модель
  async getStatusMap() {
    const result = await this.statusContract.submitTransaction('GetStatusFlow');
    return JSON.parse(result.toString());
  }

  // Установить статусную модель
  async setStatusMap() {
    const statusMap = [
      {
        name: 'В СИСТЕМЕ',
        next: ['НА УЧЕТЕ', 'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
      },
      {
        name: 'НА УЧЕТЕ',
        next: ['СНЯТО С УЧЕТА'],
      },
      {
        name: 'СНЯТО С УЧЕТА',
        next: ['НА УЧЕТЕ', 'ВЫВЕЗЕНО', 'УНИЧТОЖЕНО'],
      },
      {
        name: 'УНИЧТОЖЕНО',
        next: [],
      },
      {
        name: 'ВЫВЕЗЕНО',
        next: ['ВВЕЗЕНО'],
      },
    ];
    await this.statusContract.submitTransaction(
      'SetStatusFlow',
      JSON.stringify(statusMap),
    );
  }

  // Получить следующие статусы по статус. модели
  async getNextStatuses(currentStatus: string) {
    const result = await this.statusContract.submitTransaction(
      'GetNextStatuses',
      currentStatus,
    );
    return JSON.parse(result.toString());
  }

  // Проверка переход
  async isStatusTransitionValid(currentStatus: string, nextStatus: string) {
    const result = await this.statusContract.submitTransaction(
      'isTransitionValid',
      currentStatus,
      nextStatus,
    );
    return JSON.parse(result.toString());
  }
}
