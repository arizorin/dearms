import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BlockchainService } from './blockchain.service';

@Module({
  imports: [ConfigModule],
  providers: [ConfigService, BlockchainService],
  exports: [BlockchainService],
})
export class BlockchainModule {
  constructor() {}
}
