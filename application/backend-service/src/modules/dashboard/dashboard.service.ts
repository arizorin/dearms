import { Injectable } from '@nestjs/common';
import { BlockchainService } from '../blockchain/blockchain.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../user/entities/user.entity';
import { Repository } from 'typeorm';
import { ServiceEntity } from '../service/entities/service.entity';

@Injectable()
export class DashboardService {
  constructor(
    private blockchainService: BlockchainService,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(ServiceEntity)
    private readonly serviceRepository: Repository<ServiceEntity>,
  ) {}

  async getStats() {
    // let amountOfWeaponsTotal = 0;
    // let amountOfWeaponsInSearch = 0;
    // try {
    //   amountOfWeaponsTotal = (
    //     await this.blockchainService.queryWeapons({ selector: {} })
    //   ).length;
    //   amountOfWeaponsInSearch = (
    //     await this.blockchainService.queryWeapons({
    //       selector: { status: 'INSEARCH' },
    //     })
    //   ).length;
    // } catch (e) {
    //   console.error(e);
    // }
    const amountOfServiceTotal = await this.serviceRepository.count({});
    const amountOfServicesApplied = await this.serviceRepository.count({
      status_id: 1,
    });
    const amountOfServicesAccepted = await this.serviceRepository.count({
      status_id: 2,
    });
    const amountOfServicesRejected = await this.serviceRepository.count({
      status_id: 3,
    });

    const amountOfUsersTotal = await this.userRepository.count({});
    const amountOfCommonUsers = await this.userRepository.count({ role_id: 1 });
    const amountOfModeratorUsers = await this.userRepository.count({
      role_id: 2,
    });
    const amountOfAdminUsers = await this.userRepository.count({ role_id: 3 });

    return {
      amountOfServiceTotal,
      amountOfServicesApplied,
      amountOfServicesAccepted,
      amountOfServicesRejected,
      amountOfUsersTotal,
      amountOfCommonUsers,
      amountOfModeratorUsers,
      amountOfAdminUsers
    };
  }

  async getServiceByDate() {
    return await this.serviceRepository
      .createQueryBuilder('')
      .select(`to_char(created_at, 'DD.MM.YYYY')`, 'date')
      .addSelect('COUNT(*)', 'amount')
      .groupBy('date')
      .getRawMany();
  }

  async getServiceByStatus() {
    return await this.serviceRepository
      .createQueryBuilder('service')
      .leftJoin('service.status', 'status')
      .select('status.name')
      .addSelect('COUNT(*)', 'amount')
      .groupBy('status.name')
      .getRawMany();
  }
}
