import { Controller, Get, UseGuards } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../user/guards/roles.guard';
import { UserRole } from '../user/model/user.model';
import { Roles } from '../user/decorators/role.decorator';

@Controller('dashboard')
@ApiTags('Dashboard')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
export class DashboardController {
  constructor(private dashboardService: DashboardService) {}

  @Get('stats')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getStats() {
    return await this.dashboardService.getStats();
  }

  @Get('service-by-date')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getServiceByDate() {
    return await this.dashboardService.getServiceByDate();
  }

  @Get('service-by-status')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getServiceByStatus() {
    return await this.dashboardService.getServiceByStatus();
  }
}
