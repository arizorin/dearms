import { Injectable } from '@nestjs/common';
import { UserEntity } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { ChangeUserDto } from '../dtos/change-user.dto';
import { UserRoleEntity } from '../entities/user-role.entity';

@Injectable({})
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(UserRoleEntity)
    private readonly userRoleRepository: Repository<UserRoleEntity>,
  ) {}

  async getUserById(id: number): Promise<UserEntity> {
    return this.userRepository.findOne(
      { id: Number(id) },
      {
        relations: ['passport', 'status', 'role'],
      },
    );
  }

  async getAllUsers(): Promise<UserEntity[]> {
    return this.userRepository.find({
      relations: ['passport', 'status', 'role'],
    });
  }

  async getUsersCount(): Promise<number> {
    return this.userRepository.count();
  }

  async getAllRoles(): Promise<UserRoleEntity[]> {
    return this.userRoleRepository.find();
  }

  // Смена статуса/роли пользователя
  async changeUserStatus(
    id: number,
    data: ChangeUserDto,
  ): Promise<UpdateResult> {
    const newUser = this.userRepository.create({
      ...data,
    });

    return this.userRepository.update({ id: id }, { ...newUser });
  }
}
