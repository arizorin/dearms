import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { GetUserDto } from '../dtos/get-user.dto';
import { ChangeUserDto } from '../dtos/change-user.dto';
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { User } from '../decorators/user.decorator';
import { IUser, UserRole } from '../model/user.model';
import { Roles } from '../decorators/role.decorator';
import { RolesGuard } from '../guards/roles.guard';

@Controller('user')
@ApiTags('User')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
export class UserController {
  constructor(private userService: UserService) {}

  @Get('all')
  // @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Get('count')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getUsersCount() {
    return this.userService.getUsersCount();
  }

  @Get('roles')
  async getAllRoles() {
    return this.userService.getAllRoles();
  }

  @Get('')
  async getCurrentUser(@User() user: IUser) {
    return this.userService.getUserById(user.id);
  }

  @Get(':id')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async getUserById(@Param('id') id: number) {
    return this.userService.getUserById(id);
  }

  @Put(':id')
  @Roles(UserRole.ADMINISTRATOR, UserRole.MODERATOR)
  async changeUserStatus(
    @Param('id') id: number,
    @Body() changeUserDto: ChangeUserDto,
  ) {
    return this.userService.changeUserStatus(id, changeUserDto);
  }
}
