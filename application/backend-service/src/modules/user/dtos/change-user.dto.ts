import { IsNumber, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChangeUserDto {
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  status_id: number;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  role_id: number;
}
