import { IsNumberString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetUserDto {
  @ApiProperty()
  @IsNumberString()
  limit: number;

  @ApiProperty()
  @IsNumberString()
  skip: number;
}
