export interface IUser {
  id: number;
  role: number;
  status: number;
}

export enum UserRole {
  USER = 1,
  MODERATOR,
  ADMINISTRATOR,
}
