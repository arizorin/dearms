import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UserRoleEntity } from './entities/user-role.entity';
import { UserStatusEntity } from './entities/user-status.entity';
import { PassportEnity } from './entities/passport.enity';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      UserRoleEntity,
      UserStatusEntity,
      PassportEnity,
    ]),
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
