import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('UserStatus')
export class UserStatusEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description: string;
}
