import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('UserRole')
export class UserRoleEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description: string;
}
