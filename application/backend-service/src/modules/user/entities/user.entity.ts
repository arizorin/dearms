import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserStatusEntity } from './user-status.entity';
import { UserRoleEntity } from './user-role.entity';
import { PassportEnity } from './passport.enity';

@Entity('User')
export class UserEntity {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column({ type: 'varchar', length: 50 })
  name: string;

  @Column({ type: 'varchar', length: 50 })
  last_name: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  middle_name: string;

  @Column({ type: 'date' })
  birth_data: Date;

  @Column({ type: 'varchar', length: 20 })
  phone_number: string;

  @Column({ type: 'integer', nullable: true })
  passport_id: number;

  @ManyToOne(() => PassportEnity, (passport) => passport.id)
  @JoinColumn({ name: 'passport_id' })
  passport: PassportEnity;

  @Column({ type: 'integer', nullable: true })
  status_id: number;

  @ManyToOne(() => UserStatusEntity, (status) => status.id, { cascade: true })
  @JoinColumn({ name: 'status_id' })
  status: UserStatusEntity;

  @Column({ type: 'integer', nullable: true })
  role_id: number;

  @ManyToOne(() => UserRoleEntity, (role) => role.id)
  @JoinColumn({ name: 'role_id' })
  role: UserRoleEntity;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
