import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Passport')
export class PassportEnity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 4 })
  serial: string;

  @Column({ type: 'varchar', length: 6 })
  number: string;

  @Column({ type: 'varchar', length: 6 })
  code: string;

  @Column({ type: 'date' })
  issued_at: Date;

  @Column({ type: 'varchar', length: 255 })
  issued_by: string;
}
