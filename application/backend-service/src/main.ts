import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { version } from '../package.json';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.enableShutdownHooks();
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('DE Arms')
    .setDescription('Система децентрализованного учета оружия')
    .setVersion(version)
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  const port: number = app.get('ConfigService').get('APP_PORT');
  await app.listen(port);

  return port;
}
bootstrap().then((port) => {
  console.log(
    '\x1b[36m%s\x1b[0m',
    '[DEBUG] APPLICATION STARTED SUCCESSFULLY ON PORT: ' + port,
  );
});
