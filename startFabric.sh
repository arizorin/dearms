#!/bin/bash
set -e pipefail

starttime=$(date +%s)

pushd ./test-network
./network.sh down
./network.sh up createChannel -ca -s couchdb
./network.sh deployCC -ccn weapon -ccp ../chaincode/weapon-chaincode/ -ccl typescript -ccv 1.0
./network.sh deployCC -ccn status -ccp ../chaincode/status-chaincode/ -ccl typescript -ccv 1.0

popd

cat <<EOF

Total setup execution time : $(($(date +%s) - starttime)) secs ...

EOF
